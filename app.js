var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var dotenv = require('dotenv');
var authenticate=require('./routes/api/utils/authenticateMiddelware');

//Configuring Environemtn Variable
dotenv.config();

//Initializing Express app
var app = express();

//Requiring Routes for API
const {userRouter}=require('./routes/api/users/index');
const {houseRouter}=require('./routes/api/houses/index');
const {login}=require('./routes/api/auth/auth')


//Regquring Routes for client
const {authRouter}=require('./routes/client/auth/auth');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//Client Routes
app.use('/',authRouter)

//API Routes without authentication middleware 
app.post('/api/v1/login',login);
app.use('/api/v1/users',userRouter);

//Authentication middleware 
app.use(authenticate);

//API Routes with authentication middleware
app.use('/api/v1/house',houseRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
