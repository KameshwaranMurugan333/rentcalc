const express = require('express');
const authRouter = express.Router();

/* GET login page. */
authRouter.get('/',(req,res)=>{
    res.render('index',{title:'RentCalc'});
});

authRouter.get('/login',(req,res)=>{
    res.render('login',{title:'RentCalc-Login'});
});

authRouter.get('/signup',(req,res)=>{
    res.render('signup',{title:'RentCalc-SignUp'});
});

module.exports={authRouter};
