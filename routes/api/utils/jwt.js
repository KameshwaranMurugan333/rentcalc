const jwt=require('jsonwebtoken');

const giveToken=(payload)=>{
    var token=jwt.sign(payload,process.env.JWT_TOKEN_KEY,{
        expiresIn:'3h'
    })
    return token;
};

const verifyToken=(token)=>{
    return new Promise((resolve,reject)=>{
        try{
            var payload=jwt.verify(token,process.env.JWT_TOKEN_KEY);
            resolve(payload);
        }catch(e){
            reject(e);
        }
    });
};

module.exports={giveToken,verifyToken}