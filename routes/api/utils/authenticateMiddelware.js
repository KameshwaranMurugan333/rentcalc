const {verifyToken}=require('./jwt')

module.exports=(req,res,next)=>{
    if(req.headers.authorization){
        verifyToken(req.headers.authorization).then((obj)=>{
            next();
        },(error)=>{
            res.status(401).send({message:'Unauthorized entry',error});
        });
    }else{
        res.status(401).send({message:"Unauthorized Entry"});
    }
}