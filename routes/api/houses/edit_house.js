const models=require('./../../../models');
const {verifyToken}=require('./../utils/jwt');

var editHouse=(req,res)=>{
    verifyToken(req.headers.authorization).then((obj)=>{
        const {id,user_id,house_name,house_details}=req.body;

        if(user_id!==obj.id){
            res.status(401).send({message:'Unauthorized Operation'});
            return;
        }

        //Preparing Query
        var query=`UPDATE Houses SET house_name='${house_name}',house_details='${house_details}',user_id='${user_id}' WHERE id='${id}'`;

        //Updating the user
        models.sequelize.query(query).spread((result,metadata)=>{
            res.status(200).send(metadata);
        }).catch((error)=>{
            res.status(200).send({error});
        });
            
    },(e)=>{
        res.status(401).send({message:'Unauthorized Operation',error:e});
    })
}

module.exports={editHouse};