const express = require('express');
const houseRouter = express.Router();
const authenticate=require('./../utils/authenticateMiddelware');

//House Controller
const {addHouse}=require('./add_house');
const {editHouse}=require('./edit_house');
const {getHouseById,getHouseByUserID}=require('./get_house');
const {removeHouse}=require('./remove_house');

//Routes
houseRouter.post('/',addHouse);
houseRouter.put('/update_house',editHouse);
houseRouter.patch('/house_detail',getHouseById);
houseRouter.patch('/my_houses',getHouseByUserID);
houseRouter.delete('/remove_house',removeHouse);


module.exports={houseRouter};
