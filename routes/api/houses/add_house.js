const models=require('./../../../models');
const {verifyToken}=require('./../utils/jwt');

//Adding a house under a user
var addHouse=(req,res)=>{
    //Checking For Authorized Operation
    verifyToken(req.headers.authorization).then((obj)=>{
        const {user_id,house_name,house_details}=req.body;

        if(user_id!==obj.id){
            res.status(401).send({message:'Unathorized Operation'});
        }

        models.Houses.findOrCreate({where:{house_name:house_name},defaults:{user_id,house_name,house_details}})
        .spread((house,created)=>{
            if(created)
            {
                res.status(201).send(house);
            }else{
                res.status(200).send({message:'House name already taken!, Try Another.'});
            }
        })


    },(error)=>{
        res.status(401).send({message:'Unathorized Access',error});
    })
};

module.exports={addHouse};
