const models=require('./../../../models');
const {verifyToken}=require('./../utils/jwt');

var getHouseById=(req,res)=>{
    verifyToken(req.headers.authorization).then((obj)=>{
        const {id,user_id}=req.body;
        if(user_id!==obj.id){
            res.status(400).send({message:'Unauthorized Operation'});
        }else{
            models.Houses.findOne({where:{id:id,user_id:user_id},attributes:['id','user_id','house_name','house_details']})
            .then((house)=>{
                res.status(200).send(house);
            },(e)=>{
                res.status(404).send({message:'House not found'});
            })
        }

    },(error)=>{
        res.status(400).send({message:'Unauthorized Operation',error})
    });
};

var getHouseByUserID=(req,res)=>{
    verifyToken(req.headers.authorization).then((obj)=>{
        const {user_id}=req.body;
        if(user_id!==obj.id){
            res.status(400).send({message:'Unauthorized Operation'});
        }else{
            models.Houses.findAll({where:{user_id:user_id},attributes:['id','user_id','house_name','house_details']})
            .then((houses)=>{
                res.status(200).send(houses);
            },(e)=>{
                res.status(404).send({message:'Houses not found'});
            })
        }

    },(error)=>{
        res.status(400).send({message:'Unauthorized Operation',error})
    });
};

module.exports={getHouseById,getHouseByUserID};