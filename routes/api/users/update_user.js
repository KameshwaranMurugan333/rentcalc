const models=require('./../../../models');
const {verifyToken}=require('./../utils/jwt');
const Crypto=require('crypto-js');

//Getting has secret key
const key=process.env.PASSWORD_SECRETE_KEY;

var updateUserDetail=(req,res)=>{
    verifyToken(req.headers.authorization).then((obj)=>{
        const {full_name,mobile,id}=req.body;

        if(id!==obj.id){
            res.status(401).send({message:'Unauthorized Operation'});
            return;
        }

        //Preparing Query
        var query=`UPDATE users SET full_name='${full_name}',mobile='${mobile}' WHERE id='${obj.id}'`;

        //Updating the user
        models.sequelize.query(query).spread((result,metadata)=>{
            res.status(200).send(metadata)
        }).catch((error)=>{
            res.status(200).send({error});
        });
            
    },(e)=>{
        res.status(401).send({message:'Unauthorized Operation',error:e});
    })
};

var updateUsername=(req,res)=>{
    verifyToken(req.headers.authorization).then((obj)=>{
        const {user_name,id}=req.body;

        if(id!==obj.id){
            res.status(401).send({message:'Unauthorized Operation'});
            return;
        }

        //Checking for existence
        models.Users.findOne({where: {user_name:user_name}}).then((user)=>{
            if(user===null){
                var query=`UPDATE users SET user_name='${user_name}' WHERE id='${obj.id}'`;

                //Updating the user
                models.sequelize.query(query).spread((result,metadata)=>{
                    res.status(200).send(metadata)
                },(e)=>{
                    res.status(200).send({error:e})
                }).catch((error)=>{
                    res.status(200).send({error});
                });
            }else{
                res.status(200).send({message:'Username already taken!, Try antoher.'});
            }
        });
        
            
    },(e)=>{
        res.status(401).send({message:'Unauthorized access',e});
    })
};

var updatePassword=(req,res)=>{
    verifyToken(req.headers.authorization).then((obj)=>{
        const {old_password,new_password,id}=req.body;

        if(id!==obj.id){
            res.status(401).send({message:'Unauthorized Operation'});
            return;
        }

        //Encrypting the passwords
        var encryptOldPass=Crypto.HmacSHA1(old_password,key).toString();
        var encryptNewPass=Crypto.HmacSHA1(new_password,key).toString();

        //Checking for existence
        models.Users.findOne({where: {id:id,password:encryptOldPass}}).then((user)=>{
            if(user!==null){
                var query=`UPDATE users SET password='${encryptNewPass}' WHERE id='${obj.id}'`;

                //Updating the user
                models.sequelize.query(query).spread((result,metadata)=>{
                    res.status(200).send(metadata)
                },(e)=>{
                    res.status(200).send({error:e})
                }).catch((error)=>{
                    res.status(200).send({error});
                });
            }else{
                res.status(200).send({message:'Invalid Old Password.'});
            }
        });
        
            
    },(e)=>{
        res.status(401).send({message:'Unauthorized access',e});
    })
};

module.exports={updateUserDetail,updateUsername,updatePassword}