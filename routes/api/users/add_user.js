const models=require('./../../../models');
const _ =require('lodash');
const Crypto=require('crypto-js');
const {giveToken}=require('./../utils/jwt');

//Getting has secret key
const key=process.env.PASSWORD_SECRETE_KEY;

const addUser=(req,res)=>{
    //Getting payload from requst body
    const {email,password,full_name,mobile}=req.body;
    console.log(mobile);

    //Creating user_name from email
    var user_name=_.replace(email,'@','-');

    //Encrypting password
    var encryptPassword=Crypto.HmacSHA1(password,key).toString();    

    //Adding user  
    models.Users.findOrCreate({where: {email: email}, defaults: { email,password:encryptPassword,full_name,mobile,user_name }})
    .spread((user, created) => {
        if(created)
        {
            var resObj={
                auth_token:giveToken({
                    id:user.id,
                    email:user.email,
                    user_name:user.user_name
                }),
                id:user.id,
                message:"Success",
                status:true,
                user_name:user.user_name
            }
            res.status(201).send(resObj);
        }else{
            res.status(200).send({message:'User already exists',status:false});
        }
    })
    .catch((error)=>{
        console.log(error);
        res.status(400).send({message:'Something went wrong',status:false});
    });
  
}
        

module.exports={addUser};