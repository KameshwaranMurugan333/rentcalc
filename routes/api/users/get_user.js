const models=require('./../../../models');

//Defining function to get a single user details
const getUserById=(req,res)=>{
    var id=req.params.id;

    //Getting Single user
    models.Users.findOne({where:{id:id,is_active:true},attributes:['id','email','mobile','full_name','user_name']}).then((user)=>{
        res.status(200).send(user);
    },(e)=>{
        res.status(404).send({message:'User not found'});
    })
}

//Defining function to get all users
const getAllUsers=(req,res)=>{
    //Getting all the users in the user table
    models.Users.findAll({where:{is_active:true},attributes:['id','email','mobile','full_name','user_name']}).then((users)=>{
        res.status(200).send(users);
    },(e)=>{
        res.status(404).send({message:'Unable to get all the users ',error:e});
    })
}

module.exports={getUserById,getAllUsers}