const models=require('../../../models');
const {verifyToken}=require('../utils/jwt')

var deactivateUser=(req,res)=>{
    verifyToken(req.headers.authorization).then((obj)=>{
        const id=req.params.id;

        if(id!==obj.id){
            res.status(400).send({message:'Unauthorized Operation'});
        }else{
            //Preparing Query
            var query=`UPDATE users SET is_active=false WHERE id='${id}'`;

            //Updating the user
            models.sequelize.query(query).spread((result,metadata)=>{
                res.status(200).send(metadata)
            }).catch((error)=>{
                res.status(200).send({error});
            });
        }
    })
};

module.exports={deactivateUser};