const express = require('express');
const userRouter = express.Router();
const authenticate=require('./../utils/authenticateMiddelware');

//User Controllers
const {addUser}=require('./add_user');
const {getAllUsers,getUserById}=require('./get_user');
const {updateUserDetail,updatePassword,updateUsername}=require('./update_user');
const {deactivateUser}=require('./deactivate_user')

//Routes
userRouter.post('/',addUser);
userRouter.use(authenticate);
userRouter.get('/all',getAllUsers);
userRouter.get('/:id',getUserById);
userRouter.put('/basic_info',updateUserDetail);
userRouter.put('/update_username',updateUsername);
userRouter.put('/change_password',updatePassword);
userRouter.delete('/deactivate/:id',deactivateUser)

module.exports={userRouter};