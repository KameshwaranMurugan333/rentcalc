const models=require('./../../../models');
const Crypto=require('crypto-js');
const {giveToken}=require('./../utils/jwt')

//Getting has secret key
const key=process.env.PASSWORD_SECRETE_KEY;

var login=(req,res)=>{
    const {email,password}=req.body;

    //Encrypting password
    var encryptPassword=Crypto.HmacSHA1(password,key).toString();

    //Login in user
    models.Users.findOne({where:{email:email,password:encryptPassword},attributes:['id','user_name','full_name','email','is_active']})
    .then((user)=>{
        var resObj={
            auth_token:giveToken({
                id:user.id,
                email:user.email,
                user_name:user.user_name
            }),
            id:user.id,
            message:"Success",
            status:true,
            user_name:user.user_name
        }
        console.log('user : '+JSON.stringify(user));
        if(!user.is_active){
            models.Users.update({is_active:true},{where:{id:user.id}}).then((metadata)=>{
                console.log('metadata'+metadata);
                res.status(200).send(resObj)
            }).catch((error)=>{
                res.status(200).send({error,message:'Invalid email or password',status:false});
            });
        }else{
            res.status(200).send(resObj);
        }
    }).catch((error)=>{
        res.status(200).send({message:'Invalid email or password',status:false})
    });
}

module.exports={login};