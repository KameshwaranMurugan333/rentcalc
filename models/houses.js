'use strict';
module.exports = (sequelize, DataTypes) => {
  const Houses = sequelize.define('Houses', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },       
    house_name:{
      type:DataTypes.STRING
    }, 
    house_details: {
      type:DataTypes.STRING
    },
    is_active:{
      type:DataTypes.BOOLEAN,
      defaultValue:true
    }
  }, {});
  Houses.associate = function(models) {
    // associations can be defined here
    Houses.belongsTo(models.Users,{
      foreignKey: 'user_id',
      onDelete:'SET NULL',
      onUpdate: 'CASCADE'
    })
  };
  return Houses;
};