'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    email: {
      type:DataTypes.STRING,
      allowNull:false,      
      unique:true,
      validate:{
        isEmail:true
      }
    },
    mobile: {
      type:DataTypes.STRING,
      allowNull:true,
      // validate:{
      //   isNumeric:true,
      //   len:{
      //     args:[10,10],
      //     msg: "Mobile Number must be a 10 digit number"
      //   }
      // }
    },
    password:{ 
      type:DataTypes.STRING,
      allowNull:false
    },
    full_name:{
      type: DataTypes.STRING,
      allowNull:true
    },
    user_name:{
      type: DataTypes.STRING,
      unique:true,
      allowNull:false
    },
    is_active:{
      type: DataTypes.BOOLEAN,
      defaultValue:true
    }
  }, {});
  Users.associate = function(models) {
    // associations can be defined here
    Users.hasMany(models.Houses,{
      foreignKey: 'user_id',
      onDelete:'SET NULL',
      onUpdate: 'CASCADE'
    })
  };
  return Users;
};