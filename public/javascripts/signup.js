var signup=()=>{
    //Input Fields
    var nameObj=document.getElementById('name');
    var emailObj=document.getElementById('email');
    var passObj=document.getElementById('password');
    var confPassObj=document.getElementById('confirm_password');
    var mobObj=document.getElementById('mobile');
    var signUpobj=document.getElementById('signup_btn');

    //Error Fields
    var nameError=document.getElementById('name_error');
    var emailError=document.getElementById('email_error');
    var passwordError=document.getElementById('password_error');
    var confirmPasswordError=document.getElementById('confirm_password_error');
    var mobileError=document.getElementById('mobile_error');
    var signUpError=document.getElementById('signup_error');

    var password=passObj.value;
    var confPassword=confPassObj.value;
    var mobile=mobObj.value;

    if(!nameObj.checkValidity()){
       nameError.innerHTML=nameObj.validationMessage; 
       return;
    }else{
       nameError.innerHTML=""; 
    }

    if(!emailObj.checkValidity()){
        emailError.innerHTML="Invalid email id"; 
        return;
    }else{
        emailError.innerHTML="";
    }

    if(!passObj.checkValidity()){       
        passwordError.innerHTML="Must have 8 Character"; 
        return;
     }else{
        passwordError.innerHTML=""; 
        if(!confPassObj.checkValidity()){
            confirmPasswordError.innerHTML="Must have 8 character";
            return;
        }else if(password !== confPassword){
            confirmPasswordError.innerHTML="Confirm password doesn't match with password";
            return;
        }
        else{
            confirmPasswordError.innerHTML="";
        }
     }

     
    if(mobile.length>0 && mobile.length<10){
        mobileError.innerHTML="Invalid Mobile no";
        return;
    }else{
        mobileError.innerHTML="";
    }

    var name=nameObj.value;
    var email=emailObj.value;
    debugger;
    //Creating account
    signUpobj.innerHTML="Please wait...";
    signUpobj.disabled=true;
    axios.post(giveMeAPIurl()+'/users', {
        email: email,
        password: password,
        full_name:name,
        mobile:mobile
      })
      .then((response)=> {
          signUpobj.disabled=false;
          signUpobj.innerHTML="Get start";
          signUpError.innerHTML=response.data.message;    
          if(response.data.status===true){
            location.href='/home';
          }else{
            signUpError.innerHTML=response.data.message;       
          }
      })
      .catch((error)=> {       
        signUpobj.disabled=false;
        signUpobj.innerHTML="Get start";
        signUpError.innerHTML="Something went wrong"; 
      });
    
}