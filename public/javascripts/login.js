var login=()=>{
    var emailObj=document.getElementById('email');
    var passObj=document.getElementById('password');
    var loginError=document.getElementById('login_error');

    if(!emailObj.checkValidity()){
        document.getElementById('email_error').innerHTML="Invalid email id"; 
        return;
    }else{
        document.getElementById('email_error').innerHTML="";
    }

    if(!passObj.checkValidity()){
        document.getElementById('password_error').innerHTML=passObj.validationMessage;
        return;
    }else if(passObj.value.length<8){
        document.getElementById('password_error').innerHTML="Password must be alteast 8 character";
        return;
    }else{
        document.getElementById('password_error').innerHTML="";
    }

    var email=emailObj.value;
    var pass=passObj.value;

    document.getElementById('login_btn').innerHTML="Logging in...";
    document.getElementById('login_btn').disabled=true;

    axios.post(giveMeAPIurl()+'/login', {
        email: email,
        password: pass
      })
      .then((response)=> {
          document.getElementById('login_btn').innerHTML="Login";
          document.getElementById('login_btn').disabled=false;
          if(response.data.status===true){
            location.href='/home';
          }else{
            loginError.innerHTML=response.data.message;       
          }
      })
      .catch((error)=> {       
        document.getElementById('login_btn').innerHTML="Login";
        document.getElementById('login_btn').disabled=false;
        loginError.innerHTML="Something went wrong";        
      });
}